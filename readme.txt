using ssl

1.cd /etc/apache2/apache2.cof
add "ServerName chanceit.io"

2. cd /etc/apache2/sites-enabled/
vi 000-default.conf
<VirtualHost *:80>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com
	SetEnv SERVER_ADDRESS http://47.88.52.116
	ServerAdmin webmaster@localhost		
	DocumentRoot /var/www/html/one_yuan_backend/
	Redirect permanent / https://chanceit.io	
	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf
      
</VirtualHost>

3. cd /etc/apache2/sites-enabled/
vi default-ssl.conf
<VirtualHost *:443>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com
	SetEnv SERVER_ADDRESS http://47.88.52.116
	ServerAdmin webmaster@localhost
	ServerName chanceit.io
	DocumentRoot /var/www/html/one_yuan_backend

	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf
        SSLEngine on
        SSLCertificateFile /var/www/html/one_yuan_backend/cert/ServerCertificate.cer
        SSLCertificateKeyFile /var/www/html/one_yuan_backend/cert/private.key
        SSLCertificateChainFile /var/www/html/one_yuan_backend/cert/bundle.cer
</VirtualHost>
4. service apache2 restart
