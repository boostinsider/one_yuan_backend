<?php
namespace Home\Controller;

class UserController extends WapController{

  public function userlogin(){
    if(!defined('UID') || UID==0){// 还没登录 跳转到登录页面
      $this->error('请先登录！',U('public/login'));
    }
  }

  public function _before_index(){
    $this->userlogin();
  }
  public function index(){
	  $user=D('User')->info(UID);
	  $this->assign('user',$user);
    $this->display($this->tplpath."user.html");
  }

  public function user($id,$type=''){
  	$user=D('User')->info($id);
  	$this->assign('user',$user);
  	if($type=='lottery'){
  		$lottery=D('User')->lottery($id,$p);
  		$this->assign('lottery',$lottery);
  	}elseif($type=='displays'){
  		$displays=D('User')->displays($p,$id);
  		$this->assign('displays',$displays);
  	}else{
  		$records=D('User')->records($id,$p);
  		$this->assign('records',$records);
  	}
      $this->display($this->tplpath."user_her".($type?"_":"").$type.".html");
  }

  public function guide(){
    $this->display($this->tplpath."guide.html");
  }

  public function _before_password(){
    $this->userlogin();
  }
  public function password(){
    if(false !== D('User')->password()){
      $this->success('密码修改成功！');
    } else {
      $error = D('User')->getError();
      $this->error(empty($error) ? '未知错误！' : $error);
    }
  }

  public function records($p=1,$state='',$uid=''){
  	$uid=$uid?$uid:UID;
		$records=D('User')->records($uid,$p,$state);
		if(IS_AJAX){
			$this->ajaxReturn($records);
		}else{
			$this->assign('records',$records);
      $this->display($this->tplpath."records.html");
		}
  }

  public function lottery($p=1,$uid=''){
   	$uid=$uid?$uid:UID;
   	$lottery=D('User')->lottery($uid,$p);
   	if(IS_AJAX){
   		$this->ajaxReturn($lottery);
   	}else{
   		$this->assign('lottery',$lottery);
      $this->display($this->tplpath."lottery.html");
   	}	
  }

  public function displays($uid='',$p=1,$sid=''){
  	$displays=D('User')->displays($p,$uid,'',$sid);
  	if(IS_AJAX){
   		$this->ajaxReturn($displays);
   	}else{
   		$this->assign('displays',$displays);
      	$this->display($this->tplpath."displays.html");
   	}		
  }

  public function displays_more($id){
  	$displays=D('User')->displays_more($id);
  	$this->assign($displays);
    $this->display($this->tplpath."displays_more.html");
  }

  public function _before_shared(){
    $this->userlogin();
  }
  public function shared($pid){
    if(M('shop_period')->where('id='.$pid." and uid=".UID." and shared=1")->getField("id")){
    	if(IS_POST){
    		$res = D('User')->shared_update($pid);
  			if(false !== $res){
          $this->success('晒单成功，您获得1个欢乐币！',U('User/displays'));
        } else {
          $error = D('User')->getError();
          $this->error(empty($error) ? '未知错误！' : $error);
        }
    	}else{
    		$this->display($this->tplpath."shared.html");
    	}
    }else{
    	$this->error('请不要乱走！',U('Index/index'));
    }
  }

  public function announced($p=1,$pid=null){
  	$announced=D('User')->announced($p,$pid);
  	if(IS_AJAX){
   		$this->ajaxReturn($announced);
   	}else{
   		$this->assign('announced',$announced);
      $this->display($this->tplpath."announced.html");
   	}
  }

  public function looknum($id){
  	$period=M('shop_period')->where('id='.$id)->field('sid,no')->find();
  	$shop=M('Shop')->where('id='.$period['sid'])->field(true)->find();
  	$record = D('Shop')->user_num(I('uid'),$id);
  	$this->assign('period',$period);
  	$this->assign('shop',$shop);
  	$this->assign('record',$record);
  	$this->display($this->tplpath."user_look_num.html");
  }
	
  public function _before_address(){
    $this->userlogin();
  }
	public function address(){
		$address=D('User')->address();
    if(IS_AJAX){
      $this->ajaxReturn($address);
    }else{
      $this->assign('address',$address);
      $this->display($this->tplpath."address.html");
    }
		
  }
	
  public function _before_address_add(){
    $this->userlogin();
  }
	public function address_add(){
		if(IS_POST){
			$res = D('User')->address_update();
			if(false !== $res){
         	$this->success('新增成功！', U('address'));
      } else {
          $error = D('User')->getError();
          $this->error(empty($error) ? '未知错误！' : $error);
      }
		}else{
			$user=D('User')->info(UID);
			$this->assign('user',$user);
			$this->assign('cart',I('get.cart'));
			$this->meta_title = '新增地址';
			$this->display($this->tplpath."address_add.html");
		}
  }
	
  public function _before_address_edit(){
    $this->userlogin();
  }
	public function address_edit($id){
		if(IS_POST){
			 if(false !== D('User')->address_update()){
          $this->success('编辑成功！', U('address'));
        } else {
          $error = D('User')->getError();
          $this->error(empty($error) ? '未知错误！' : $error);
        }
		}else{
			$info=D('User')->address_info($id);
			$user=D('User')->info(UID);
			$this->assign('info',$info);
			$this->assign('user',$user);
			$this->meta_title = '编辑地址';
			$this->display($this->tplpath."address_add.html");
		}
  }
	
  public function _before_address_del(){
    $this->userlogin();
  }
	public function address_del(){
    $id = array_unique((array)I('id',0,'intval'));
    if ( empty($id) ) {
        $this->error('请选择要操作的数据!');
    }
    $map = array('id' => array('in', $id));
    if(M('shop_address')->where($map)->delete()){
        $this->success('删除成功');
    } else {
        $this->error('删除失败！');
    }
  }

  public function _before_address_default(){
    $this->userlogin();
  }
  public function address_default($id){
    D('User')->address_default(UID);
		M('shop_address')->where('id='.$id)->setField('default',1);
	}
  public function _before_set_username(){
    $this->userlogin();
  }
  public function set_username(){
    M('user')->where('id='.UID)->setField('nickname',I('nickname'));
    $auth = array(
      'uid'             => UID,
      'username'        => I('nickname')
    );
    session('user_auth', $auth);
    session('user_auth_sign', data_auth_sign($auth));
  }

  public function _before_recharge_list(){
    $this->userlogin();
  }
  public function recharge_list($p='',$paydate=''){
    $list=D('User')->recharge_list($p,$paydate);
    if(IS_AJAX){
      $this->ajaxReturn($list);
    }else{
      $this->assign('list',D('User')->recharge_list());
      $this->display($this->tplpath."recharge_list.html");
    }
  }


  //-------------------------app endpoint--------------------

  public function app_userlogin(){
    if(!defined('UID') || UID==0){// 还没登录 跳转到登录页面
      $data['status']=401;
      $data['message']="login first";
      $this->ajaxReturn($data);
    }
  }

  //现在揭晓
  public function app_announced($page=1,$pid=null){
    $announced=D('User')->announced($page,$pid);
    $data['status'] = 200;
    $data['data'] = $announced;
    $this->ajaxReturn(  $data);

  }
  //recharge
  public function _before_app_recharge_list()
  {
    $this->app_userlogin();
  }
  public function app_recharge_list($page='',$paydate='',$page_size=5){
    $list=D('User')->recharge_list($page,$paydate,$page_size);
    $data['status']=200;
    $data['data']=$list;
      $this->ajaxReturn($data);

  }
  //user info
  public function _before_app_get()
  {
    $this->app_userlogin();
  }
  public function app_get()
  {
    $user = D('User')->info(UID);
    $data['status']=200;
    $data['data']=$user;
    $this->ajaxReturn($data);
  }

  public function app_user($id,$type=''){
    $user=D('User')->info($id);
    $this->assign('user',$user);
    if($type=='lottery'){
      $lottery=D('User')->lottery($id,$p);
      $this->assign('lottery',$lottery);
      $this->ajaxReturn($lottery);

    }elseif($type=='displays'){
      $displays=D('User')->displays($p,$id);
      $this->ajaxReturn($displays);
      $this->assign('displays',$displays);
    }else{
      
      $records=D('User')->records($id,$p);
      $this->assign('records',$records);
      $this->ajaxReturn($user);
    }
  }

  //购买记录 state = 0 进行中，1，即将进行，2已经揭晓
  public function _before_app_records()
  {
    $this->app_userlogin();
  }
  public function app_records($page=null,$state=null,$page_size=5)
  {
    $records = D('User')->records(UID,$page,$state,$page_size);
    $data['status']=200;
    $data['data']=$records;
    $this->ajaxReturn($data);
  }

  // add address
    public function _before_app_address_add(){
    $this->app_userlogin();
  }
  public function app_address_add($nickname=null,$tel=null,$province=null,$city=null,$address){
    // if(IS_POST)
    {
      $res = D('User')->address_update();
      if(false !== $res){
          // $this->success('新增成功！', U('address'));
        // echo "success";
        $data['status']=200;
        $this->ajaxReturn($data);
      } else {
          $error = D('User')->getError();
          // echo $error;
          // $this->error(empty($error) ? '未知错误！' : $error);
          $data['status']=400;
          $data['message']=$error;
          $this->ajaxReturn($data);
      }
    }
  }


  public function _before_app_address_edit(){
    $this->userlogin();
  }
  public function app_address_edit($id){
       if(false !== D('User')->address_update()){
          $data['status']=200;
          $this->ajaxReturn($data);
        } else {
          $error = D('User')->getError();
          $data['status']=400;
          $data['message']=$error;
          $this->ajaxReturn($data);
        }    
  }


  // default address

  public function _before_app_address_default(){
    $this->userlogin();
  }
  public function app_address_default($id){
    D('User')->address_default(UID);
    $res = M('shop_address')->where('id='.$id)->setField('default',1);
    $data['status']=200;
    $data['data']= $res;
    $this->ajaxReturn($data);
  }

  //get address
    public function _before_app_address(){
    $this->app_userlogin();
  }
  public function app_address(){
    $address=D('User')->address();   
    $data['status'] = 200;
    $data['data'] = $address;  
    $this->ajaxReturn($data);    
  }

  //get lottery
  public function app_lottery($uid,$page=1,$page_size=10){    
      $lottery=D('User')->lottery($uid,$page,$page_size);
      $data['status']=200;
      $data['data']=$lottery;
      $data['page']=$page;
      $this->ajaxReturn($data);
  }

  //看你的number
  public function app_looknum($pid)
  {
    $period=M('shop_period')->where('id='.$pid)->field('sid,no')->find();
    $shop=M('Shop')->where('id='.$period['sid'])->field(true)->find();
    $record = D('Shop')->user_num(UID,$pid);

    $data['status'] = 200;
    $data['period'] = $period;
    $data['shop'] = $shop;
    $data['record'] = $record;

    $this->ajaxReturn($data);

  }

  //晒单
    public function _before_app_shared(){
    $this->app_userlogin();
  }
  public function app_shared($pid){
    if(M('shop_period')->where('id='.$pid." and uid=".UID." and shared=1")->getField("id"))
    {
      if(IS_POST)
      {
        $res = D('User')->app_shared_update($pid);
        $data['res'] = $res;
        if(false !== $res){
          // $this->success('晒单成功，您获得1个欢乐币！',U('User/displays'));

          $data['status']=200;
          $data['data'] = "晒单成功，您获得1个欢乐币";
          $this->ajaxReturn($data);

        } else {
          $error = D('User')->getError();
          $data['status']=400;
          $data['message']=$error;
          $this->ajaxReturn($data);
        }
      }
    }
  }
  //晒单列表
  public function _before_app_share_list()
  {
    // $this->app_userlogin();
  }
  public function app_share_list($uid='',$sid='',$page=1,$page_size=10)
  {
      $displays=D('User')->displays($page,$uid,$page_size,$sid);
      $data['status'] = 200;
      $data['data'] = $displays;
      $this->ajaxReturn($data);
      // $this->assign('displays',$displays);
  }
  //晒单详情
  public function app_share_more($id)
  {
    $displays=D('User')->displays_more($id);
    $data['status'] = 200;
    $data['data'] = $displays;
    $this->ajaxReturn($data);
  }

  //edit profile
  public function _before_app_edit_profile(){
    $this->userlogin();
  }
  public function app_edit_profile(){
    $nickname = I('nickname');
    $sex = I('sex');
    $birthday = I('birthday');
    $bio = I('bio');
    $headimgurl = I('headimgurl');
    if ($nickname)
    {
      $res0=M('user')->where('id='.UID)->setField('nickname',I('nickname'));
      $auth = array(
        'uid'             => UID,
        'username'        => I('nickname')
      );
      session('user_auth', $auth);
      session('user_auth_sign', data_auth_sign($auth));
    }
    if ($sex)
    {
      $res1 = M('user')->where('id='.UID)->setField('sex',$sex);
    }
    if ($birthday)
    {
     $res2 = M('user')->where('id='.UID)->setField('birthday',$birthday);
    }
    if ($bio)
    {
      $res3 = M('user')->where('id='.UID)->setField('bio',$bio);
    }
    if ($headimgurl)
    {
      $res4 = M('user')->where('id='.UID)->setField('headimgurl',$headimgurl);
    }

    $data['status']=200;
    $this->ajaxReturn($data);
  }

}