use onepay;
alter table hx_shop_address add zip varchar(50);

--0613  user profile
alter table hx_user add birthday varchar(100);
alter table hx_user add bio varchar (100);
alter table hx_user modify column sex varchar(20) default 'male';


--0620 translate
update hx_web_menu set title='Manage Products' , `group` = 'Shopping Mall' where id = 69;
update hx_web_menu set title='Add Products' , `group` = 'Shopping Mall' where id = 70;
update hx_web_menu set title='Edit Products' , `group` = 'Shopping Mall' where id = 71;

update hx_web_menu set title='Products Category' , `group` = 'Shopping Mall' where id = 154;
update hx_web_menu set title='Add Category' , `group` = 'Shopping Mall' where id = 155;
update hx_web_menu set title='Edit Category' , `group` = 'Shopping Mall' where id = 156;

update hx_web_menu set title='Settings' where id = 36;

update hx_web_menu set title='Manage Users' ,`group` = 'User' where id = 146;
update hx_web_menu set title='Edit Users' ,`group` = 'User' where id = 147;
update hx_web_menu set title='Purchase Record' ,`group` = 'User' where id = 148;
update hx_web_menu set title='Change Password' ,`group` = 'User' where id = 150;
update hx_web_menu set title='Change Password' ,`group` = 'User' where id = 54;

update hx_web_menu set title='Award Record' ,`group` = 'Award' where id = 54;