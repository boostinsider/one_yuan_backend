(function() {
    var ThinkPHP = window.Think;

    // $('.test').click(function() {
    //     var nonce = $('input[name="payment_method_nonce"]').val();
    //     alert(nonce);
    //     $.post(ThinkPHP.U("pay/test"), {
    //         nonce:nonce
    //     }).success(function(data) {
            
    //         console.log(data);
    //         debugger;

    //     });
    // })

    $('.key-block').on('click',function(e){

        $('.key-block').removeClass('selected');
        $(this).addClass('selected');

    });

    getToken(this);
    $('.get-payment-info').click(function(){
        getToken(this);
    });

    function getToken(event) {
        $.blockUI();
        $.post(ThinkPHP.U("pay/get_client_token"), {}).success(function(data) {
            console.log(data);
            var token = data.token;

            // alert(token);
            braintree.setup(token, "dropin", {
                container: "payment-form",
                onReady:function(integration){
                    $.unblockUI();
                },
                paymentMethodNonceReceived:function(event,nonce)
                {
                    console.log(nonce);
                    pay(nonce);
                }
            });

        });
    }
    function pay(nonce)
    {
        var amount = $('.key-block.selected').val();
        $.blockUI();
        $.post(ThinkPHP.U("pay/pay"), {
            amount:amount,
            nonce,nonce
        }).success(function(data) {
            $.unblockUI();
            if (data.status == 200)
            {
                var black = data.data.black;
                $("#black").html(black);
                alert('success!');
                window.location.href='onepay://'
            }
            else
            {
                if (data.status == 501)
                    alert("Please don't submit twice!");
                else
                    alert('Purchase failure!')
            }

        });
    }

    function yuePay(event) {
        $.post(ThinkPHP.U("pay/yuepay"), {
            pid: $('input[name="pid"]').val(),
            price: $('input[name="price"]').val()
        }).success(function(data) {
            if (data.url) {
                location.href = data.url;
            } else {
                $(event).prop('disabled', false);
            }
        });
    }

    //调用微信JS api 支付
    function weixinPay(event) {
        if (!$('input[name="price"]').val()) {
            layer.msg('请选择充值金额！', { icon: 2 });
            $(event).prop('disabled', false);
            return false;
        }
        $.get(ThinkPHP.U("pay/pay_weixin", { "price": $('input[name="price"]').val(), "pid": $('input[name="pid"]').val() })).success(function(status) {
            if (status.status) {
                if (status.info.return_msg != "OK") {
                    layer.msg(status.info.return_msg, { icon: 2, shade: 0.8 });
                    $(event).prop('disabled', false);
                    return false;
                }
                StandardPost(ThinkPHP.U("pay/pay_wx"), { code: status.info.code_url, price: status.info.price, no: status.info.no, pid: status.info.pid });
            }
        });
    }

    function StandardPost(url, args) {
        var form = $("<form method='post'></form>");
        form.attr({ "action": url });
        for (arg in args) {
            var input = $("<input type='hidden'>");
            input.attr({ "name": arg });
            input.val(args[arg]);
            form.append(input);
        }
        form.submit();
    }

    function aliPay(event) {
        layer.confirm('<h3>请在新开窗口完成支付！</h3>完成付款后根据您的情况进行以下操作！', {
            icon: 0,
            btn: ['完成支付', '遇到问题，重新选择'] //按钮
        }, function(index) {
            location.href = ThinkPHP.U('user/index');
        }, function(index) {
            layer.close(index);
            $(event).prop('disabled', false);
        });
        $('form').attr('action', ThinkPHP.U('pay/pay_alipay')).attr('target', '_blank').submit();
    }

    function bankPay(event, pay_type) {
        layer.confirm('<h3>请在新开窗口完成支付！</h3>完成付款后根据您的情况进行以下操作！', {
            icon: 0,
            btn: ['完成支付', '遇到问题，重新选择'] //按钮
        }, function(index) {
            location.href = ThinkPHP.U('user/index');
        }, function(index) {
            layer.close(index);
            $(event).prop('disabled', false);
        });
        $('form').append("<input value=" + pay_type + " name='pay_type' type='hidden'>");
        $('form').attr('action', ThinkPHP.U('pay/pay_bank')).attr('target', '_blank').submit();
    }

    function callpay() {
        if (typeof WeixinJSBridge == "undefined") {
            if (document.addEventListener) {
                document.addEventListener('WeixinJSBridgeReady', weixinPay, false);
            } else if (document.attachEvent) {
                document.attachEvent('WeixinJSBridgeReady', weixinPay);
                document.attachEvent('onWeixinJSBridgeReady', weixinPay);
            }
        } else {
            weixinPay();
        }
    }

    $('.dollar').click(function() {
        $('.dollar').removeClass('active');
        $(this).addClass('active');
        $('input[name="price"]').val($(this).not(':contains("其他金额")').text());
    });

    $('input[name="price_ye"]').keyup(function() {
        $('input[name="price"]').val($(this).val());
    });

    $('.pay-title b').click(function() {
        $('.pay-title b').removeClass('active');
        $(this).addClass('active');
        var index = $(".pay-title b").index(this);
        $('.pay-nav').hide();
        $('.pay-nav').eq(index).show();
    });

    $('.pay-nav h6').click(function() {
        if ($(this).children('i').hasClass('icon-angle-down')) {
            $(this).children('i').removeClass('icon-angle-down').addClass('icon-angle-up').text(' 收起部分银行');
            $('.pay-nav:visible').find('.pay-label').show();
        } else {
            $(this).children('i').removeClass('icon-angle-up').addClass('icon-angle-down').text(' 展开更多银行');
            $('.pay-nav:visible').find('.pay-label:gt(9)').hide();
        }
    });

    $('.pay-label').click(function() {
        $('.pay-label').removeClass('active');
        $(this).addClass('active');
    });

    $('.pay-nav').find('.pay-label:gt(9)').hide();
    $('.pay-nav:not(:eq(' + $(".pay-title b.active").index() + '))').hide();
}());
